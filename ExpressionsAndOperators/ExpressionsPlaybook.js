//Test bitwise operators
// Print the binary form of a number
console.log(Number(113).toString(2))

// convert binary to decimal
console.log(parseInt(10,2))
console.log(parseInt(100,2))
console.log(parseInt(1000,2))
console.log(parseInt(10000,2))
console.log(parseInt(100000,2))
