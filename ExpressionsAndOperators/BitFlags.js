// Set flags and check if it is set or not
//var FLAG1 = 0x00000001
//var FLAG2 = 0x00000002
//var FLAG3 = 0x00000004
// var FLAG4 = 0x00000008
// var FLAG5 = 0x00000032
var NOFLAG = 0x00000000
var FLAG1 = 1 << 0  // 1
var FLAG2 = 1 << 1  // 2
var FLAG3 = 1 << 2  // 4
var FLAG4 = 1 << 3  // 8
var FLAG5 = 1 << 4  // 16
var FLAG6 = 1 << 5  // 32

var FLAGGED_STATE = FLAG2 | FLAG1 | FLAG5
var _flag = NOFLAG

function addFlags(f) {
    _flag |= f
}

function hasFlag() {
  return (_flag & FLAGGED_STATE) > 0;
}

// Set restricted state of a security with flags and compare it with flagged state to
// know if the instrument can be traded
console.log("Add FLAG3")
addFlags(FLAG3)
if ( hasFlag() ) {
  console.log("FLAG3 is set")
} else {
  console.log("FLAG3 is NOT set")
}

console.log("Add FLAG4")
addFlags(FLAG4)
if (hasFlag()) {
  console.log("FLAG4 is set")
} else {
  console.log("FLAG4 is NOT set")
}

console.log("Add FLAG6")
addFlags(FLAG6)
if (hasFlag()) {
  console.log("FLAG6 is set")
} else {
  console.log("FLAG6 is NOT set")
}

console.log("Add FLAG5")
addFlags(FLAG5)
if (hasFlag()) {
  console.log("FLAG5 is set")
} else {
  console.log("FLAG5 is NOT set")
}

console.log("Add FLAG2")
addFlags(FLAG2)
if (hasFlag()) {
  console.log("FLAG2 is set")
} else {
  console.log("FLAG2 is NOT set")
}
