// function expression - immediately invoked - one time use
var storage = (function(x) {return Math.pow(2, x);}(4));
console.log(storage)

// nested functions


// recursion with function expressions
var f = function fact(x) { if (x <= 1) return 1; else return x*fact(x -1); };
console.log("Factorial of 4 is: " + f(4));
