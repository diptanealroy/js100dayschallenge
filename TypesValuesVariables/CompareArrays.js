function equalArray(a,b) {
  if ( a.length != b.length) return false;
  for (var i = 0; i < a.length; i++) {
    if ( a[i] !== b[i]) return false;
  }
  return true;
}

var a = [1, 8, 3];
var b = [1,2,3];
var result = equalArray(a, b)
console.log("The comparison result is " + result);
