/*
* Reverse an array using recursion
*/

function reverseArray(arr, rArr, fwd, back) {
  if ( back < 0 ) {
    return;
  } else {
    rArr[fwd++] = arr[back--];
    reverseArray(arr, rArr, fwd, back);
  }
}

var array = [1, 2, 3, 4, 5];
var rArray = [];

reverseArray(array, rArray, 0, array.length - 1);
console.log(...rArray);
