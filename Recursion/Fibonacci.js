/*
* Print the fibonacci series for n numbers
* Note: fib series nth item is sum of n-1 and n-2th items
*/

function fibonacci(n) {
  if (n <= 1) {
    return 1;
  } else if ( n == 2) {
    return 1;
  } else {
    return fibonacci(n - 2) + fibonacci(n - 1);
  }
}


for (var i =1; i < 8; i++) {
  console.log(" " + fibonacci(i));
}
