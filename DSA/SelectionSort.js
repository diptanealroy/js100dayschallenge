// Selection sorting using temp Array
// Book: The Algorithm Design Manual - Skiena
/*
* Psuedo-code
* SelectionSort(A)
*   For i = 1 to n do
*     Sort(i) = Find Min from arr
*     Delete Min from arr
*     Return (Sort)
*/
function findMinimum( arr ) {
  var min = arr[0];
  var pos = 0;
  for (var j = 0; j < arr.length; j++ ) {
      if ( min > arr[j]) {
        min = arr[j];
        pos = j;
      }
  }
  arr.splice(pos,1);
  return min;
}

function selectionSort(array) {
  var sortedArray = [];
  for (var i = 0, j =0; i < array.length;j++) {
      sortedArray[j] = findMinimum(array);
  }
  return sortedArray;
}

var selectionArray = [14, 10, 9, 11, 7, 8];
var sorted = selectionSort(selectionArray)
console.log(...sorted);
