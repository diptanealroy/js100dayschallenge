/*
* Book: Algorithm Design Manual - Skiena
* Implemenation of Quick sorted
*/
function swap(array, low, high) {
  var temp = array[high];
  array[high] = array[low];
  array[low] = temp;
}

function partition (array, low, high ) {
  var pivot = high;
  var pIndex = low;

  for (var i=low; i < high; i++) {
    if (array[i] < array[pivot]) {
      swap(array, i, pIndex);
      pIndex++;
    }
  }
  swap(array, pIndex, high);
  return pIndex;
}

function quickSort( array, low, high ) {
  if ( low < high ) {
    var pivot = partition(array, low, high);
    quickSort(array, low, pivot - 1);
    quickSort(array, pivot + 1, high);
  }
}

var array = [9,5,1,8,7];
quickSort(array, 0, array.length -1 );
console.log(...array);
