/*
* Quick Sort Implementation
*/

function swap (arr, l, h) {
  var temp = arr[h];
  arr[h] = arr[l];
  arr[l] = temp;
}

function partition(arr, low, high) {
  var pivot = arr[low];
  var i = low, j = high + 1;

  while ( i < j) {
    do {
      i++;
    } while (arr[i] < pivot && i < high);

    do {
      j--;
    } while (arr[j] > pivot);

    if ( i < j) {
      swap(arr, i, j);
    }
  }

  arr[low] = arr[j];
  arr[j] = pivot;
  return j;
}

function quickSort(array, low, high) {
  if ( low < high ) {
    var pivot = partition(array, low, high);
    quickSort(array, low, pivot - 1);
    quickSort(array, pivot + 1, high);
  }
}

var arr = [9,5,1,8,7];
quickSort(arr, 0, arr.length -1 );
console.log(...arr);
