/*
* This example will perform Selection Sort without using
* temp array
*/

function selectionSort(array) {
  var pos = -1;
  var temp = 0;
  var swap = false;

  for ( var i = 0; i < array.length - 1; i++ ) {
    pos = i;
    swap = false;
    for ( var j = i+1; j < array.length; j++){
      if ( array[j] < array[pos]) {
        pos = j;
        swap = true;
      }
    }

    if ( swap ){
      temp = array[pos];
      array[pos] = array[i];
      array[i] = temp;
    }
  }
}

var arr = [14, 10, 9, 11, 7, 8];
console.log("Prior to sorting ")
console.log(...arr)
selectionSort(arr)
console.log("After sorting ")
console.log(...arr)
